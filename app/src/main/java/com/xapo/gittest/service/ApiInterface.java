package com.xapo.gittest.service;

import com.xapo.gittest.model.RepositoriesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface ApiInterface {

    @GET("search/repositories?q=android%20language:java&sort=stars&order=desc")
    Call<RepositoriesResponse> getAndroidTrendingRepoSync(@Query("page") int page, @Query("per_page") int per_page);

}
