package com.xapo.gittest.viewmodel;

import android.util.Log;

import com.xapo.gittest.model.RepositoriesResponse;
import com.xapo.gittest.service.ApiClient;
import com.xapo.gittest.service.ApiInterface;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class ListViewModel extends ViewModel {

    public int perPage = 100;
    private ApiInterface apiService;
    private Retrofit retrofit;
    private String TAG = getClass().getName();

    public ListViewModel() {
        retrofit = new ApiClient().getClient();
        apiService = retrofit.create(ApiInterface.class);
    }

    public LiveData<RepositoriesResponse> getAndroidTrendingRepo(int page) {
        final MutableLiveData<RepositoriesResponse> data = new MutableLiveData<>();

        Call<RepositoriesResponse> call = apiService.getAndroidTrendingRepoSync(page, perPage);
        call.enqueue(new Callback<RepositoriesResponse>() {
            @Override
            public void onResponse(Call<RepositoriesResponse> call, retrofit2.Response<RepositoriesResponse> response) {
                if (response.isSuccessful()) {
                    data.setValue(response.body());
                } else {
                    data.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<RepositoriesResponse> call, Throwable t) {
                Log.e(TAG, "Unable to submit request to API.");
                data.setValue(null);
            }
        });
        return data;
    }
}
