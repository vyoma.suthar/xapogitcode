package com.xapo.gittest.ui.fragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.xapo.gittest.R;
import com.xapo.gittest.model.Item;
import com.xapo.gittest.model.RepositoriesResponse;
import com.xapo.gittest.ui.adpater.RepositoryListAdapter;
import com.xapo.gittest.viewmodel.ListViewModel;

import java.util.Objects;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.xapo.gittest.utility.XapoUtilities.isConnectedToNetwork;
import static com.xapo.gittest.utility.XapoUtilities.showProgress;

public class ListFragment extends Fragment implements RepositoryListAdapter.onRepositoryClickListener {

    protected static final String FEED_TAG = "feed";
    private ListViewModel mViewModel;
    private View view;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RepositoryListAdapter mAdapter;
    protected static final String SELECTED_REPO = "selected Repository";
    private View mProgressView;
    private int nextPage = 0;
    private boolean isLast;

    public static ListFragment newInstance() {
        return new ListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.list_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ListViewModel.class);
        initView();
    }

    private void initView() {

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView_repository);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mProgressView = view.findViewById(R.id.progress);

        mAdapter = new RepositoryListAdapter(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(getContext(), R.drawable.line_divider)));
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RecyclerView.LayoutManager layoutManager = mRecyclerView.getLayoutManager();
                if (dy > 0) {
                    if ((mLayoutManager.getChildCount() + ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition()) >= layoutManager.getItemCount() && !isLast) {
                        fetchRepositoryList(nextPage);
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isConnectedToNetwork(getContext(), mRecyclerView)) {
            mAdapter.clearData();
            fetchRepositoryList(0);
        }
    }

    public void fetchRepositoryList(int page) {
        showProgress(getContext(), mProgressView, true);

        mViewModel.getAndroidTrendingRepo(page).observe(this, new Observer<RepositoriesResponse>() {
            @Override
            public void onChanged(RepositoriesResponse response) {
                if (response != null) {
                    mAdapter.addData(response.getItems());
                    isLast = response.getItems().size() == mViewModel.perPage ? false : true;
                    nextPage++;
                } else {
                    Toast.makeText(getContext(), getString(R.string.issue_fetching_data), Toast.LENGTH_SHORT).show();
                }
                showProgress(getContext(), mProgressView, false);
            }
        });
    }

    @Override
    public void onRepositoryClick(Item item) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        DetailFragment fragment = DetailFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putSerializable(SELECTED_REPO, item);
        fragment.setArguments(bundle);
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(FEED_TAG);
        transaction.commit();
    }
}
