package com.xapo.gittest.ui.adpater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xapo.gittest.R;
import com.xapo.gittest.model.Item;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RepositoryListAdapter extends RecyclerView.Adapter<RepositoryListAdapter.RepositoryHolder> {
    private ArrayList<Item> repoItem;
    private onRepositoryClickListener listener;

    public interface onRepositoryClickListener {
        void onRepositoryClick(Item item);
    }

    public RepositoryListAdapter(ArrayList<Item> dataSet) {
        repoItem = dataSet;
    }

    public RepositoryListAdapter(onRepositoryClickListener listener) {
        this.listener = listener;
        repoItem = new ArrayList<Item>();
    }


    @NonNull
    @Override
    public RepositoryListAdapter.RepositoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row = inflater.inflate(R.layout.repository_item, parent, false);
        return new RepositoryHolder(row);
    }

    @Override
    public void onBindViewHolder(RepositoryHolder holder, int position) {

        final Item repositoryDetail = repoItem.get(position);
        RepositoryHolder repositoryHolder = (RepositoryHolder) holder;
        repositoryHolder.title.setText(repositoryDetail.getFullName());
        repositoryHolder.description.setText(repositoryDetail.getDescription());
        repositoryHolder.star.setText(String.valueOf(repositoryDetail.getStargazersCount()));

        repositoryHolder.root_ll.setOnClickListener(v -> {
            listener.onRepositoryClick(repositoryDetail);
        });
    }

    @Override
    public int getItemCount() {
        if (repoItem != null)
            return repoItem.size();
        else
            return 0;
    }


    public void addData(List<Item> data) {
        if (data.isEmpty()) {
            return;
        }
        repoItem.addAll(data);
        notifyDataSetChanged();
    }

    public void clearData() {
        if (repoItem != null && repoItem.size() > 0) {
            int size = repoItem.size();
            repoItem.clear();
            notifyItemRangeRemoved(0, size);
        }
    }

    public class RepositoryHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView description;
        private TextView star;
        private View root_ll;

        public RepositoryHolder(View itemView) {
            super(itemView);
            root_ll = itemView.findViewById(R.id.root_ll);
            title = itemView.findViewById(R.id.textView_title);
            description = itemView.findViewById(R.id.textView_description);
            star = itemView.findViewById(R.id.textView_star);
        }
    }

}