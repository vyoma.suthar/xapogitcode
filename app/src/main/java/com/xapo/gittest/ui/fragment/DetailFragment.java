package com.xapo.gittest.ui.fragment;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xapo.gittest.R;
import com.xapo.gittest.model.Item;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import static com.xapo.gittest.ui.fragment.ListFragment.FEED_TAG;
import static com.xapo.gittest.ui.fragment.ListFragment.SELECTED_REPO;

public class DetailFragment extends Fragment {

    private View view;
    private TextView title;
    private TextView description;
    private TextView star;
    private TextView url;
    private Item repoDetail;
    private ImageView ownerImage;
    private TextView ownerName;

    public static DetailFragment newInstance() {
        return new DetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.detail_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        setUpListeners();
    }


    private void initView() {

        title = view.findViewById(R.id.textView_title);
        description = view.findViewById(R.id.textView_description);
        star = view.findViewById(R.id.textView_star);
        url = view.findViewById(R.id.textView_url);
        ownerImage = view.findViewById(R.id.imageView_owner);
        ownerName = view.findViewById(R.id.textView_owner_name);

        Bundle args = getArguments();
        if (args != null) {
            repoDetail = (Item) args.getSerializable(SELECTED_REPO);
            showAllData(repoDetail);
        }


    }

    private void setUpListeners() {
        view.findViewById(R.id.back_textView).setOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }


    private void showAllData(Item repoDetail) {
        title.setText(repoDetail.getFullName());
        description.setText(repoDetail.getDescription());
        star.setText(String.valueOf(repoDetail.getStargazersCount()));
        url.setText(repoDetail.getHtmlUrl());
        url.setMovementMethod(LinkMovementMethod.getInstance());

        if (repoDetail.getOwner() != null) {
            ownerName.setText(repoDetail.getOwner().getLogin());
            Glide.with(getContext()).load(repoDetail.getOwner().getAvatarUrl()).into(ownerImage);
        }
    }
}
